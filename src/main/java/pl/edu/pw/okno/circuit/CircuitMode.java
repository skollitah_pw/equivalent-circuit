package pl.edu.pw.okno.circuit;

public enum CircuitMode {
  ADMITTANCE,
  IMPEDANCE
}
