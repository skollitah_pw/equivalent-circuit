package pl.edu.pw.okno.circuit.argument;

import com.beust.jcommander.Parameter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EvaluateCircuitArgs {

  public static final String NAME = "evaluate";

  @Parameter(
      names = "-m",
      description = "Mode: 'A' - admittance, 'I' - impedance",
      required = true,
      arity = 1)
  private String mode;

  @Parameter(names = "-d", description = "Circuit definition", required = true, arity = 1)
  private String circuitDefinition;

  @Parameter(names = "-p", description = "Return result in polar coordinates")
  private boolean polar;
}
