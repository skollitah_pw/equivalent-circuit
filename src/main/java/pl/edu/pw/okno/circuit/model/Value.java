package pl.edu.pw.okno.circuit.model;

import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.math3.complex.Complex;

@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
@ToString
public class Value implements Element {
  private final Complex value;
  private final Type type;

  public static Value from(Literal inputChar, double leftOperand, double rightOperand) {
    if (inputChar.getCharacter().equals(Literal.IMPEDANCE_LEFT)
        || inputChar.getCharacter().equals(Literal.IMPEDANCE_RIGHT)) {
      return new Value(new Complex(leftOperand, rightOperand), Type.IMPEDANCE);
    } else if (inputChar.getCharacter().equals(Literal.ADMITTANCE_LEFT)
        || inputChar.getCharacter().equals(Literal.ADMITTANCE_RIGHT)) {
      return new Value(new Complex(leftOperand, rightOperand), Type.ADMITTANCE);
    } else {
      throw new IllegalArgumentException();
    }
  }

  public Value add(Value second) {
    if (this.getType().equals(second.getType())) {
      return new Value(this.getValue().add(second.getValue()), this.getType());
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public boolean isPartOfValue() {
    return false;
  }

  @Override
  public boolean isCircuitTopologyOperator() {
    return false;
  }

  @Override
  public boolean isSeparator() {
    return false;
  }

  @Override
  public boolean isLeftParenthesis() {
    return false;
  }

  @Override
  public boolean isValueRightParenthesis() {
    return false;
  }

  @Override
  public boolean isStopCharacter(Set<Literal> stopCharacters) {
    return false;
  }

  @Override
  public boolean isRightClosing(Element leftStarting) {
    return false;
  }

  @Override
  public boolean isParallel() {
    return false;
  }

  @Override
  public boolean isSerial() {
    return false;
  }

  @Override
  public boolean isRightParenthesis() {
    return false;
  }

  public enum Type {
    ADMITTANCE,
    IMPEDANCE
  }
}
