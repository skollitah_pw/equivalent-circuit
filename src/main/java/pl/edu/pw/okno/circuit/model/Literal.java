package pl.edu.pw.okno.circuit.model;

import com.google.common.collect.Sets;
import java.util.Map;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
@ToString
public class Literal implements Element {
  public static final Character ADMITTANCE_LEFT = '<';
  public static final Character ADMITTANCE_RIGHT = '>';
  public static final Character IMPEDANCE_LEFT = '[';
  public static final Character IMPEDANCE_RIGHT = ']';
  public static final Character CIRCUIT_LEFT = '(';
  public static final Character CIRCUIT_RIGHT = ')';
  public static final Character PARALLEL = '|';
  public static final Character SERIAL = '_';
  public static final Character SEPARATOR = ',';

  public static Map<Character, Character> FROM_LEFT =
      Map.of(
          Literal.ADMITTANCE_LEFT,
          Literal.ADMITTANCE_RIGHT,
          Literal.IMPEDANCE_LEFT,
          Literal.IMPEDANCE_RIGHT,
          Literal.CIRCUIT_LEFT,
          Literal.CIRCUIT_RIGHT);
  public static Map<Character, Character> FROM_RIGHT =
      Map.of(
          Literal.ADMITTANCE_RIGHT,
          Literal.ADMITTANCE_LEFT,
          Literal.IMPEDANCE_RIGHT,
          Literal.IMPEDANCE_LEFT,
          Literal.CIRCUIT_RIGHT,
          Literal.CIRCUIT_LEFT);
  public static final Set<Literal> LEFT_PARENTHESES =
      Sets.newHashSet(
          new Literal(ADMITTANCE_LEFT),
          new Literal(IMPEDANCE_LEFT),
          new Literal(Literal.CIRCUIT_LEFT));
  public static final Set<Literal> RIGHT_PARENTHESES =
      Sets.newHashSet(
          new Literal(ADMITTANCE_RIGHT), new Literal(IMPEDANCE_RIGHT), new Literal(CIRCUIT_RIGHT));

  private final Character character;

  @Override
  public boolean isPartOfValue() {
    return character >= '0' && character <= '9' || character == '.' || character == '-';
  }

  @Override
  public boolean isCircuitTopologyOperator() {
    return PARALLEL.equals(character) || Literal.SERIAL.equals(character);
  }

  @Override
  public boolean isSeparator() {
    return SEPARATOR.equals(character);
  }

  @Override
  public boolean isLeftParenthesis() {
    return LEFT_PARENTHESES.contains(new Literal(character));
  }

  @Override
  public boolean isValueRightParenthesis() {
    return ADMITTANCE_RIGHT.equals(character) || IMPEDANCE_RIGHT.equals(character);
  }

  @Override
  public boolean isRightClosing(Element leftStarting) {

    return leftStarting instanceof Literal
        && FROM_RIGHT.get(character).equals(((Literal) leftStarting).getCharacter());
  }

  @Override
  public boolean isStopCharacter(Set<Literal> stopCharacters) {
    return stopCharacters.contains(new Literal(character));
  }

  @Override
  public boolean isParallel() {
    return PARALLEL.equals(character);
  }

  @Override
  public boolean isSerial() {
    return SERIAL.equals(character);
  }

  @Override
  public boolean isRightParenthesis() {
    return CIRCUIT_RIGHT.equals(character);
  }
}
