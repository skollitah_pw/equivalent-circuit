package pl.edu.pw.okno.circuit;

import static pl.edu.pw.okno.circuit.EvaluationException.invalidCharacter;
import static pl.edu.pw.okno.util.StringUtils.removeWhitespaces;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.complex.Complex;
import pl.edu.pw.okno.circuit.model.Element;
import pl.edu.pw.okno.circuit.model.Literal;
import pl.edu.pw.okno.circuit.model.Value;

@Slf4j
@RequiredArgsConstructor
public class Evaluator {

  private final CircuitMode circuitMode;
  private final boolean polar;
  private final Deque<Element> circuitModelChars = new ArrayDeque<>();

  public Complex evaluate(String circuitModelInput) {
    Preconditions.checkArgument(
        !Strings.isNullOrEmpty(circuitModelInput), "circuitModelText is null or empty");

    return evaluateInternal(removeWhitespaces(circuitModelInput).toCharArray());
  }

  private Complex evaluateInternal(char[] circuitModelInput) {

    for (int i = 0; i < circuitModelInput.length; i++) {
      Literal inputChar = new Literal(circuitModelInput[i]);

      if (inputChar.isLeftParenthesis()
          || inputChar.isPartOfValue()
          || inputChar.isSeparator()
          || inputChar.isCircuitTopologyOperator()
          || inputChar.isCircuitTopologyOperator()) {

        circuitModelChars.push(inputChar);
      } else if (inputChar.isRightParenthesis()) {
        Value rightValue = (Value) circuitModelChars.pop();
        Element leftStarting = circuitModelChars.pop();

        if (leftStarting == null || !inputChar.isRightClosing(leftStarting)) {
          throw invalidCharacter(inputChar.getCharacter(), i);
        }

        Value newValue = applyTopology(rightValue);

        circuitModelChars.push(newValue);
      } else if (inputChar.isValueRightParenthesis()) {
        double rightOperand = popRightOperand();
        circuitModelChars.pop();
        double leftOperand = popLeftOperand();

        Element leftParenthesis = circuitModelChars.pop();

        if (leftParenthesis == null || !inputChar.isRightClosing(leftParenthesis)) {
          throw invalidCharacter(inputChar.getCharacter(), i);
        }

        Value rightValue = Value.from(inputChar, leftOperand, rightOperand);
        Value newValue = applyTopology(rightValue);
        circuitModelChars.push(newValue);
      }
    }

    if (circuitModelChars.size() != 1) {
      throw new IllegalStateException();
    }

    Complex result = ((Value) circuitModelChars.pop()).getValue();

    return polar ? Equivalent.toPolar(result) : result;
  }

  private Value calculateSerial(Value leftValue, Value rightValue) {
    Value leftValueImpedance = Equivalent.toImpedance(leftValue);
    Value rightValueToImpedance = Equivalent.toImpedance(rightValue);
    Value summaryImpedance = leftValueImpedance.add(rightValueToImpedance);

    if (CircuitMode.ADMITTANCE.equals(circuitMode)) {
      return Equivalent.toAdmittance(summaryImpedance);
    } else if (CircuitMode.IMPEDANCE.equals(circuitMode)) {
      return summaryImpedance;
    } else {
      throw new IllegalStateException();
    }
  }

  private Value calculateParallel(Value leftValue, Value rightValue) {
    Value leftValueAdmittance = Equivalent.toAdmittance(leftValue);
    Value rightValueAdmittance = Equivalent.toAdmittance(rightValue);
    Value summaryAdmittance = leftValueAdmittance.add(rightValueAdmittance);

    if (CircuitMode.ADMITTANCE.equals(circuitMode)) {
      return summaryAdmittance;
    } else if (CircuitMode.IMPEDANCE.equals(circuitMode)) {
      return Equivalent.toImpedance(summaryAdmittance);
    } else {
      throw new IllegalStateException();
    }
  }

  private double popLeftOperand() {
    return popOperand(Literal.LEFT_PARENTHESES);
  }

  private double popRightOperand() {
    return popOperand(Sets.newHashSet(new Literal(Literal.SEPARATOR)));
  }

  private double popOperand(Set<Literal> stopCharacters) {
    Deque<Character> operandCharacters = new ArrayDeque<>();

    while (circuitModelChars.peek() != null
        && !circuitModelChars.peek().isStopCharacter(stopCharacters)) {
      operandCharacters.push(((Literal) circuitModelChars.pop()).getCharacter());
    }

    return new BigDecimal(toString(operandCharacters)).doubleValue();
  }

  private String toString(Collection<Character> characters) {
    StringBuilder sb = new StringBuilder();

    for (Character character : characters) {
      sb.append(character);
    }

    return sb.toString();
  }

  private Value applyTopology(Value rightValue) {
    Value newValue;

    if (circuitModelChars.peek() != null && circuitModelChars.peek().isCircuitTopologyOperator()) {
      Literal circuitTopologyOperator = (Literal) circuitModelChars.pop();
      Value leftValue = (Value) circuitModelChars.pop();

      if (circuitTopologyOperator.isParallel()) {
        newValue = calculateParallel(leftValue, rightValue);
      } else if (circuitTopologyOperator.isSerial()) {
        newValue = calculateSerial(leftValue, rightValue);
      } else {
        throw new IllegalStateException();
      }

    } else {
      if (CircuitMode.ADMITTANCE.equals(circuitMode)) {
        newValue = Equivalent.toAdmittance(rightValue);
      } else {
        newValue = Equivalent.toImpedance(rightValue);
      }
    }

    return newValue;
  }
}
