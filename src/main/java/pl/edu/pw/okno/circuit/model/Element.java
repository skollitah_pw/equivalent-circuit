package pl.edu.pw.okno.circuit.model;

import java.util.Set;

public interface Element {

  boolean isPartOfValue();

  boolean isCircuitTopologyOperator();

  boolean isSeparator();

  boolean isLeftParenthesis();

  boolean isValueRightParenthesis();

  boolean isRightClosing(Element leftStarting);

  boolean isStopCharacter(Set<Literal> stopCharacters);

  boolean isParallel();

  boolean isSerial();

  boolean isRightParenthesis();
}
