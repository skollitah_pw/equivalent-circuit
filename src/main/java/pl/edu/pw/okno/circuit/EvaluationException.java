package pl.edu.pw.okno.circuit;

public class EvaluationException extends RuntimeException {

  public EvaluationException(String message) {
    super(message);
  }

  public static EvaluationException invalidCharacter(Character character, int position) {
    return new EvaluationException(
        String.format("Invalid character - [%s] - on position - [%d]", character, position));
  }
}
