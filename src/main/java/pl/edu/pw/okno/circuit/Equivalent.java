package pl.edu.pw.okno.circuit;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.math3.complex.Complex;
import pl.edu.pw.okno.circuit.model.Value;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Equivalent {

  public static Value toImpedance(Value value) {
    return Value.Type.ADMITTANCE.equals(value.getType())
        ? new Value(toImpedance(value.getValue()), Value.Type.IMPEDANCE)
        : value;
  }

  public static Value toAdmittance(Value value) {
    return Value.Type.IMPEDANCE.equals(value.getType())
        ? new Value(toAdmittance(value.getValue()), Value.Type.ADMITTANCE)
        : value;
  }

  public static Complex toImpedance(Complex admittance) {
    return calculate(admittance);
  }

  public static Complex toAdmittance(Complex impedance) {
    return calculate(impedance);
  }

  public static Complex toPolar(Complex complexValue) {
    double realValue = complexValue.getReal();
    double imaginaryValue = complexValue.getImaginary();

    double newRealValue = Math.sqrt(Math.pow(realValue, 2.0) + Math.pow(imaginaryValue, 2.0));
    double newImaginaryValue = Math.atan(imaginaryValue / realValue);

    return Complex.valueOf(newRealValue, newImaginaryValue);
  }

  private static Complex calculate(Complex complexValue) {
    double realValue = complexValue.getReal();
    double imaginaryValue = complexValue.getImaginary();

    double divisor = Math.pow(realValue, 2.0) + Math.pow(imaginaryValue, 2.0);
    double newRealValue = realValue / divisor;
    double newImaginaryValue = -imaginaryValue / divisor;

    return Complex.valueOf(newRealValue, newImaginaryValue);
  }
}
