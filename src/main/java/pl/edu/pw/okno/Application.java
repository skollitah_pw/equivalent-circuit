package pl.edu.pw.okno;

import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.complex.Complex;
import pl.edu.pw.okno.circuit.CircuitMode;
import pl.edu.pw.okno.circuit.Evaluator;
import pl.edu.pw.okno.circuit.argument.EvaluateCircuitArgs;

@Slf4j
public class Application {

  public static void main(String[] args) {
    EvaluateCircuitArgs evaluateCircuitArgs = new EvaluateCircuitArgs();

    JCommander jc = JCommander.newBuilder().addObject(evaluateCircuitArgs).build();

    jc.parse(args);

    handleArgs(evaluateCircuitArgs);
  }

  private static void handleArgs(EvaluateCircuitArgs evaluateCircuitArgs) {
    CircuitMode circuitMode;
    if ("A".equals(evaluateCircuitArgs.getMode())) {
      circuitMode = CircuitMode.ADMITTANCE;
    } else if ("I".equals(evaluateCircuitArgs.getMode())) {
      circuitMode = CircuitMode.IMPEDANCE;
    } else {
      throw new IllegalArgumentException();
    }

    Evaluator evaluator = new Evaluator(circuitMode, evaluateCircuitArgs.isPolar());

    Complex result = evaluator.evaluate(evaluateCircuitArgs.getCircuitDefinition());

    printResult(result, circuitMode, evaluateCircuitArgs.isPolar());
  }

  private static void printResult(Complex result, CircuitMode circuitMode, boolean polar) {
    String coordinates = polar ? "polar" : "cartesian";
    String mode = CircuitMode.ADMITTANCE.equals(circuitMode) ? "admittance" : "impedance";
    log.info("Result {} in {} coordinates is - [{}]", mode, coordinates, result);
  }
}
