package pl.edu.pw.okno.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StringUtils {

  public static String removeWhitespaces(String str) {
    return str.replaceAll("\\s", "");
  }
}
