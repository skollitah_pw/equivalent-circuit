package pl.edu.pw.okno.circuit

import org.apache.commons.math3.complex.Complex
import org.assertj.core.api.Assertions
import org.assertj.core.data.Offset
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class EvaluatorTest extends Specification {

    def evaluator = new Evaluator(CircuitMode.ADMITTANCE, false)

    def 'should evaluate circuit definition: #definition with admittance evaluator'() {
        given:
            def service = new Evaluator(CircuitMode.ADMITTANCE, polar)
        when:
            Complex result = service.evaluate(definition)
        then:
            Assertions.assertThat(result.getReal()).isCloseTo(resultReal, Offset.offset(0.01d));
        where:
            definition                                          || resultReal || resulImaginary || polar
            '<21.5, 34>'                                         | 21.5d       | 34d             | false
            '<0, -1>'                                            | 0d          | -1d             | false
            '<-1, 0>'                                            | -1d         | 0d              | false
            '<21.5, 34>'                                         | 40.2275d    | 1.007d          | true
            '<0, -1>'                                            | 1d          | -1.571d         | true
            '<-1, 0>'                                            | 1d          | 3.142d          | true
            '<-1.432, 0.312> | <34.4, 75.12>'                    | 32.968d     | 75.432d         | false
            '(<-1.432, 0.312>) | <34.4, 75.12>'                  | 32.968d     | 75.432d         | false
            '((<-1.432, 0.312>) | (<34.4, 75.12>))'              | 32.968d     | 75.432d         | false
            '[8.45, -1.512]'                                     | 0.115d      | 0.021d          | false
            '([1.432, 0.312] _ (<34.4, 75.12> | [0.345, 3.54]))' | 0.667d      | -0.14d          | false
    }

    def 'should evaluate circuit definition: #definition with impedance evaluator for cartesian coordinates'() {
        given:
            def service = new Evaluator(CircuitMode.IMPEDANCE, polar)
        when:
            Complex result = service.evaluate(definition)
        then:
            Assertions.assertThat(result.getReal()).isCloseTo(resultReal, Offset.offset(0.01d));
        where:
            definition                                          || resultReal || resulImaginary || polar
            '[11.034, 0.434]'                                    | 11.034d     | 0.434d          | false
            '[0, -1]'                                            | 0d          | -1d             | false
            '[-1, 0]'                                            | -1d         | 0d              | false
            '[11.034, 0.434]'                                    | 11.043d     | 0.039d          | true
            '[0, -1]'                                            | 1d          | -1.571d         | true
            '[-1, 0]'                                            | 1d          | 3.142d          | true
            '[-9.532, 18.312] _ [34.4, -75.12]'                  | 24.868d     | -56.808d        | false
            '([-9.532, 18.312]) _ [34.4, -75.12]'                | 24.868d     | -56.808d        | false
            '(([-9.532, 18.312]) _ [34.4, -75.12])'              | 24.868d     | -56.808d        | false
            '<0.545, 3.34>'                                      | 0.048d      | -0.292d         | false
            '([1.432, 0.312] _ (<34.4, 75.12> | [0.345, 3.54]))' | 1.437d      | 0.301d          | false
    }

    def 'should throw IllegalArgumentException if input null or empty'() {
        when:
            evaluator.evaluate(definition)
        then:
            thrown(IllegalArgumentException)
        where:
            definition << [
                null,
                ''
            ]
    }
}

