package pl.edu.pw.okno

import spock.lang.Specification

class ApplicationTest extends Specification {

    def 'should run application'() {
        given:
            def args = [
                '-p',
                '-m',
                'I',
                '-d',
                '([-1.432, 0.312] _ (<34.4, 75.12> | [0.345, 3.54]))'
            ] as String[]
        expect:
            Application.main(args)
    }
}
